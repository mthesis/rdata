import numpy as np
import os
from os import path as op




bp="../wdata/jet_4ext_mul2qq/"


files=[int(q[:q.find(".")]) for q in os.listdir(bp)]

files.sort()

files=[bp+str(q)+".npz" for q in files]


hasc=[]
hass=[]
hasb=[]
hasmu=[]
hastau=[]

allps=[]
alljet=[]

for file in files:
  f=np.load(file,allow_pickle=True)
  pp=f["particles"]
  jj=f["jetsAll"]

  for p,j in zip(pp,jj):
    ps=list(set([q[0] for q in p]))

    hasc.append(4 in ps or -4 in ps)
    hass.append(3 in ps or -3 in ps)
    hasb.append(5 in ps or -5 in ps)
    hasmu.append(13 in ps or -13 in ps or 14 in ps or -14 in ps)
    hastau.append(15 in ps or -15 in ps or 16 in ps or -16 in ps)
    
    allps.append(ps)
    alljet.append(j)

  print("did file",file)

print("saving generall")
np.savez_compressed("quark",q=alljet,p=allps,charm=hasc,strange=hass,bottom=hasb,mu=hasmu,tau=hastau)

hasc=np.array(hasc)
hass=np.array(hass)
hasb=np.array(hasb)
hasmu=np.array(hasmu)
hastau=np.array(hastau)

alljet=np.array(alljet)

print("saving bottom")
i=np.where(hasb)
np.savez_compressed("quark_bottom",q=alljet[i])


print("saving charm")
i=np.where(np.logical_and(hasc,~hasb))
np.savez_compressed("quark_charm",q=alljet[i])


print("saving strange")
i=np.where(np.logical_and(hass,~hasc,~hasb))
np.savez_compressed("quark_strange",q=alljet[i])


print("saving tau")
i=np.where(hastau)
np.savez_compressed("quark_tau",q=alljet[i])


print("saving mu")
i=np.where(np.logical_and(~hastau,hasmu))
np.savez_compressed("quark_mu",q=alljet[i])






